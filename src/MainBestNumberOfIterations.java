import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

import helperClasses.IOHelper;
import classify.Evaluation;
import classify.MultilabelPerceptron;
import dataClasses.Data;


public class MainBestNumberOfIterations {
	
	public static void main(String[] args) {
		
		boolean geneExtraction = true;
		boolean posTagging = false;
		
		if (posTagging) {
			String trainingDataFile = "/home/users0/klingern/teaching/team-lab-ss2015/data/pos/train.col";
			String developmentDataFile =  "/home/users0/klingern/teaching/team-lab-ss2015/data/pos/dev-predicted.col";
			String resultsFile = "/home/users0/rothmube/teamlab/data/iterationExperiment/resultsPOS_development.col";

			IOHelper ioHelper = new IOHelper();
			MultilabelPerceptron mp = new MultilabelPerceptron(true);
			
			
			try {
				Data trainingsdata = ioHelper.readFiles(trainingDataFile, trainingDataFile);
				PrintStream printStream = new PrintStream(new File("/home/users0/rothmube/git/teamlab_cl_01/experiments/iterations/perceptronPOS.txt"));
				
				for(int i=1; i<=50; i++ ){
					System.out.println("\nComputing with " + i + " number of iterations (Pos Tagging)\n");
					
					// train model with i iterations
					mp.trainPerceptron(trainingsdata, i);
					mp.saveModel("/home/users0/rothmube/git/teamlab_cl_01/models/iterationExperiment/perceptronPOS_features.txt", "/home/users0/rothmube/git/teamlab_cl_01/models/iterationExperiment/perceptronModelPOS.txt");
					
					// classify development data
					Data developmentData = ioHelper.readTokenFile(developmentDataFile);
					developmentData = mp.classifyData(developmentData);
					ioHelper.saveData(resultsFile, developmentData, false);
					
					// evaluate classification
					Data evaluateData = ioHelper.readFiles(developmentDataFile, resultsFile);
					Evaluation eval = evaluateData.calculateEvaluation();
					
					// save to file
					// Number of iterations + Precision + Recall + F-Score + Accuracy
					printStream.println(i + "\t" + eval.getMacroPrecision() + "\t" + eval.getMacroRecall() 
							+ "\t" + eval.getMacroFScore() + "\t" + eval.getAccuracy());
				}

			} catch (IOException e) {
				// TODO Automatisch generierter Erfassungsblock
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Automatisch generierter Erfassungsblock
				e.printStackTrace();
			}

		}
		
		if (geneExtraction) {
			String trainingDataFile = "/home/users0/rothmube/teamlab/data/GeneExtraction/80_train.iob";
			String developmentDataFile =  "/home/users0/rothmube/teamlab/data/GeneExtraction/20_train_development.iob";
			String resultsFile = "/home/users0/rothmube/teamlab/data/iterationExperiment/resultsGene_development.iob";
			
			IOHelper ioHelper = new IOHelper();
			MultilabelPerceptron mp = new MultilabelPerceptron(false);
			
			try {
				Data trainingsdata = ioHelper.readFiles(trainingDataFile, trainingDataFile);
				PrintStream printStream = new PrintStream(new File("/home/users0/rothmube/git/teamlab_cl_01/experiments/iterations/perceptronGenes.txt"));
				
				for(int i=1; i<=50; i++ ){
					System.out.println("\nComputing with " + i + " number of iterations (Gene Extraction)\n");
					
					// train model with i iterations
					mp.trainPerceptron(trainingsdata, i);
					mp.saveModel("/home/users0/rothmube/git/teamlab_cl_01/models/iterationExperiment/perceptronGenes_features.txt", "/home/users0/rothmube/git/teamlab_cl_01/models/iterationExperiment/perceptronModelGenes.txt");
					
					// classify development data
					Data developmentData = ioHelper.readTokenFile(developmentDataFile);
					developmentData = mp.classifyData(developmentData);
					ioHelper.saveData(resultsFile, developmentData, false);
					
					// evaluate classification
					Data evaluateData = ioHelper.readFiles(developmentDataFile, resultsFile);
					Evaluation eval = evaluateData.calculateEvaluation();
					
					// save to file
					// Number of iterations + Precision + Recall + F-Score + Accuracy
					printStream.println(i + "\t" + eval.getMacroPrecision() + "\t" + eval.getMacroRecall() 
							+ "\t" + eval.getMacroFScore() + "\t" + eval.getAccuracy());
				}

			} catch (IOException e) {
				// TODO Automatisch generierter Erfassungsblock
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Automatisch generierter Erfassungsblock
				e.printStackTrace();
			}
		}
		
	}

}
