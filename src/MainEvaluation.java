import helperClasses.IOHelper;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

import classify.DecisionTree;
import classify.Evaluation;
import classify.FeatureExtraction;
import classify.MultilabelPerceptron;
import dataClasses.Data;
import dataClasses.Sentence;
import dataClasses.Token;


public class MainEvaluation {

	public static void main(String[] args) {
		//String trainingData = "/home/users0/klingern/teaching/team-lab-ss2015/data/pos/train.col";
		String trainingData = "data/train.col";
		String goldFile = "data/dev.col";
		//String predFile = "/home/users0/klingern/teaching/team-lab-ss2015/data/pos/dev-predicted.col";
		String predFile = "data/dev-predicted.col";
		//String predFile = "data/emptyPred.txt";
		//String predFile = "data/NNPred.txt";
				
		try {
		
		IOHelper ioHelper = new IOHelper();
		Data trainingsdata = ioHelper.readFiles(trainingData, trainingData);
		
		//trainingsdata = ioHelper.readFiles(goldFile, goldFile);
		
		Data data = ioHelper.readTokenFile("data/dev.col");
		Data dataTraining = ioHelper.readTokenFile("data/train.col");
		Data dataTesting = ioHelper.readTokenFile("data/test-nolabels.col");
		
		boolean isPerceptron = true;
		boolean isDecisionTree = false;
		
		//Evaluation eval = data.calculateEvaluation();
		
		//FeatureExtraction features = new FeatureExtraction();
		//features.extractFeatures(data, true);
		
		//eval.printEvaluation();
		
		if (isPerceptron) {
			MultilabelPerceptron mp = new MultilabelPerceptron(true);
			mp.trainPerceptron(trainingsdata, 100);
			//mp.trainPerceptron(trainingsdata, 1);
			mp.saveModel("models/features.txt", "models/perceptronModel.txt");
			try {
				data = mp.classifyData(data);
				dataTraining = mp.classifyData(dataTraining);
				dataTesting = mp.classifyData(dataTesting);
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if (isDecisionTree) {
			DecisionTree dt = new DecisionTree();
			dt.trainDecisionTree(trainingsdata, 0.0, 1000, "MI");
			dt.saveDecisionTree("models/decisionTreeModel.ser");
			
			// load Model
			dt = null;
			try {
				dt = ioHelper.loadDecisionTreeModel("models/decisionTreeModel.ser");
			} catch (ClassNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			data = dt.classify(data);
		}
		
		// save classified data
		try {
			//data = mp.classifyData(data);
			/*Token token = data.getData().get(0).getTokens().get(0);
			data = new Data();
			Sentence sentence = new Sentence();
			sentence.addToken(token);
			data.addSentence(sentence);*/
			ioHelper.saveData("data/results_development.col", data, false);
			ioHelper.saveData("data/results_train.col", dataTraining, false);
			ioHelper.saveData("data/results_test.col", dataTesting, false);
			
		} catch (Exception e) {
			// TODO Automatisch generierter Erfassungsblock
			e.printStackTrace();
		}
		
		System.out.println("Development Set: ");
		// evaluate classification
		Data evaluateData = ioHelper.readFiles("data/dev.col", "data/results_development.col");
		//Data evaluateData = ioHelper.readFiles("data/train.col", "data/results.col");
		Evaluation eval = evaluateData.calculateEvaluation();
		eval.printEvaluation();
		
		System.out.println("\nTraining Set: ");
		evaluateData = ioHelper.readFiles("data/train.col", "data/results_train.col");
		//Data evaluateData = ioHelper.readFiles("data/train.col", "data/results.col");
		eval = evaluateData.calculateEvaluation();
		eval.printEvaluation();
		
//		DTNode node = dt.rootNode;
//		System.out.println("Tokens: " + node.data.size() + " Feature: " + node.featureID + " Label: " + node.label);
//		node = node.leftChild;
//		System.out.println("Tokens: " + node.data.size() + " Feature: " + node.featureID + " Label: " + node.label);
//		node = node.rightChild;
//		System.out.println("Tokens: " + node.data.size() + " Feature: " + node.featureID + " Label: " + node.label);
//		
//		dt.printTree();
		
//		for (Sentence sentence: data.getData()) {
//			for (Token token: sentence.getTokens()) {
//				System.out.println(token.getWordform() + "\t" + token.getGoldTag() + "\t" + token.getPredTag() + "\t" + token.getFeatureVector());
//			}
//		}
		
//		Sentence sentence = data.getData().get(11);
//		for (Token token: sentence.getTokens()) {
//			System.out.println(token.getWordform() + "\t" + token.getGoldTag() + "\t" + token.getPredTag());
//		}
//
//		int[] tpFpFn;
//		for (String label: sentence.getTpFpFn().keySet()) {
//			tpFpFn = sentence.getTpFpFn().get(label);
//			System.out.println(label + ": " + tpFpFn[0] + " " + tpFpFn[1] + " " + tpFpFn[2]);
//		}
		

		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}

}
