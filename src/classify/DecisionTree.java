package classify;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;

import dataClasses.Data;
import dataClasses.Sentence;
import dataClasses.Token;

public class DecisionTree implements Serializable {
	
	private DTNode rootNode;
	private FeatureExtraction featureMap;
	
	public void trainDecisionTree(Data data, String splittingCriteria) {
		// compute the feature vectors for the data
		featureMap = new FeatureExtraction();
		featureMap.extractFeatures(data, true);
		
		rootNode = new DTNode(data, new ArrayList<Integer>(), featureMap.getNumFeatures(), splittingCriteria);
		// start creation of the decision tree
		rootNode.divideData();
	}
	
	public void trainDecisionTree(Data data, double informationGainThreshold, 
								  int maxTokensLeaf, String splittingCriteria) {
		// compute the feature vectors for the data
		featureMap = new FeatureExtraction();
		featureMap.extractFeatures(data, true);
		
		rootNode = new DTNode(data, new ArrayList<Integer>(), featureMap.getNumFeatures(), splittingCriteria);
		rootNode.setInformationGainThreshold(informationGainThreshold);
		rootNode.setMaxTokensLeaf(maxTokensLeaf);
		
		// start creation of the decision tree
		rootNode.divideData();
	}
	
	public Data classify(Data data) {
		featureMap.extractFeatures(data, false);
		
		ArrayList<Sentence> sentenceList = data.getData();
		ArrayList<Token> tokenList;
		String predictedLabel;
		Token token;
		
		// iterate over each sentence
		for (int i = 0; i < sentenceList.size(); i++) {
			tokenList = sentenceList.get(i).getTokens();
			
			// iterate over each token
			for(int j = 0 ; j < tokenList.size() ; j++){
				token = tokenList.get(j);
				// classify token
				predictedLabel = rootNode.classifyToken(token.getFeatureVector());
				token.setPredTag(predictedLabel);
				// update data with the token containing the predicted label
				data.updateToken(i, j, token);
			}
		}
		
		return data;
	}
	
	public void saveDecisionTree(String fileName) {
		FileOutputStream file;
		BufferedOutputStream buffer;
		ObjectOutput outputStream;
		
		try {
			file = new FileOutputStream(fileName);
			buffer = new BufferedOutputStream(file);
			outputStream = new ObjectOutputStream(buffer);
			
			// write decision tree model to file
			outputStream.writeObject(this);
			
			// close file
			outputStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	}
	
}
