package classify;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import dataClasses.Data;
import dataClasses.Sentence;
import dataClasses.Token;


public class FeatureExtraction implements Serializable {
	protected Map<String, Integer> featureMap;
	protected Map<String, Integer> labelMap;
	
	// initialize class without a specific feature and label mapping
	public FeatureExtraction() {
		featureMap = new HashMap<String, Integer>();
		labelMap = new HashMap<String, Integer>();
	}
	
	// initialize class with the feature and label mapping specified in the given file
	public FeatureExtraction(String featureLabelFile) {
		featureMap = new HashMap<String, Integer>();
		labelMap = new HashMap<String, Integer>();
		this.loadFeatureMapping(featureLabelFile);
	}

	// return the feature mapping
	public Map<String, Integer> getFeatureMap() {
		return featureMap;
	}
	
	// add a label with the next integer as id
	protected void addLabel(String label) {
		if (!labelMap.containsKey(label)) {
			labelMap.put(label, labelMap.size());
		}
	}
	
	// add a label with the specified id
	private void addLabel(String label, Integer id) {
		labelMap.put(label, id);
	}
	
	// add a feature with the specified id
	private void addFeature(String feature, Integer id) {
		featureMap.put(feature, id
				);
	}
	
	// get the full name of the label with the specified id
	public String getLabel(int label) {
		String labelName = null;
		for (Map.Entry<String, Integer> entry: labelMap.entrySet()) {
			if (entry.getValue() == label) {
				labelName = entry.getKey();
				break;
			}
		}
		
		return labelName;
	}
	
	// get the id of the specified label name
	public int getLabel(String label) {
		return labelMap.get(label);
	}
	
	// get the number of labels in the label mapping
	public int getNumLabels() {
		return labelMap.size();
	}
	
	// get the number of features in the feature mapping
	public int getNumFeatures() {
		return featureMap.size();
	}
	
	
	/*
	 * method creates the feature vector for the token given and its context within the sentence
	 */
	private ArrayList<Integer> getFeatureVector(int tokenPosition, Token token, Sentence sentence, boolean isTrained) {
		String feature;
		String wordform;
		Integer index;
		ArrayList<String> featureList = new ArrayList<String>();
		ArrayList<Integer> featureVector = new ArrayList<Integer>();
		
		// switches for the different features
		boolean useWordform = true;
		
		boolean useWordformBefore = true;
		boolean useWordformAfter = true;
		
		boolean useSuffixes = true;
		boolean usePrefixes = true;
		
		// test only one or both
		boolean useShape = false;
		boolean useCollapsedShape = false;
		
		boolean useVowelConsonantPattern = false; 	// could help in identifying foreign words
		
		// test only one and all three
		boolean useStartsWithCapital = true;
		boolean useCapitalLetterAndMidSentence = true;
		boolean useContainsUpperCase = true;
		
		boolean useAllUpperCase = false;
		
		boolean useContainsNumber = false;
		boolean useContainsHyphen = true;
		boolean useContainsPeriod = false;
		
	
		wordform = token.getWordform();
		
		feature = "defaultFeature";
		featureList.add(feature);
		
		// wordform of the token
		if (useWordform) {
			feature = "wordform_" + wordform.toLowerCase();
			featureList.add(feature);
		}
		
		
		// Starts with capital letter
		if (useStartsWithCapital) {
			if(Character.isUpperCase(wordform.charAt(0))){
				feature = "wordform_StartsWithCapital";
				featureList.add(feature);
			}
		}
		
		if (useCapitalLetterAndMidSentence) {
			// Starts with capital letter and is not at the start of the sentence
			if (tokenPosition == 0 && Character.isUpperCase(wordform.charAt(0))) {
				feature = "CapitalLetterAndMidSentence";
				featureList.add(feature);
			}
		}
		
		if (useContainsUpperCase) {
			// Contains uppercase character
			if (wordform.matches(".*[A-Z].*")) {
				feature = "ContainsUpperCase";
				featureList.add(feature);
			}
		}
		
		if (useContainsNumber) {
			// Contains number
			if (wordform.matches("\\d")) {
				feature = "ContainsNumber";
				featureList.add(feature);
			}
		}
		
		if (useContainsHyphen) {
			// Contains hyphen
			if (wordform.contains("-")) {
				feature = "ContainsHyphen";
				featureList.add(feature);
			}
		}
		
		if (useContainsPeriod) {
			// contains period
			if (wordform.contains(".") && !wordform.equals(".")) {
				feature = "ContainsPeriod";
				featureList.add(feature);
			}
		}
		
		if (useShape) {
			String shape = wordform;
			shape = shape.replaceAll("[A-Z]", "A");
			shape = shape.replaceAll("[a-z]", "a");
			shape = shape.replaceAll("[0-9]", "0");
			shape = shape.replaceAll("[^Aa0]", "_");
			feature = "Shape=" + shape;
			featureList.add(feature);
		}
		
		if (useCollapsedShape) {
			String collapsedShape = wordform;
			collapsedShape = collapsedShape.replaceAll("[A-Z]", "A");
			collapsedShape = collapsedShape.replaceAll("[a-z]", "a");
			collapsedShape = collapsedShape.replaceAll("[0-9]", "0");
			collapsedShape = collapsedShape.replaceAll("[^Aa0]", "_");
			collapsedShape = collapsedShape.replaceAll("AAA+", "AAA");
			collapsedShape = collapsedShape.replaceAll("aaa+", "aaa");
			collapsedShape = collapsedShape.replaceAll("000+", "000");
			collapsedShape = collapsedShape.replaceAll("___+", "___");
			feature = "CollapsedShape=" + collapsedShape;
			featureList.add(feature);
		}
		
		if (useAllUpperCase) {
			// All letters uppercase (may contain dots)
			if (wordform.matches("([A-Z]*\\.)*[A-Z]+([A-Z]*\\.)*")) {
			//if (wordform.matches("\\b([A-Z]+\\.?)+\\b")) {		--> forgets the period at the end
				feature = "AllUpperCase";
				//System.out.println(wordform);
				featureList.add(feature);
			}
		}
		
		if (useSuffixes) {
			// Suffix features
			for (int i = 2; i <= 5; i++) {
				if (wordform.length() > i) {
					feature = Integer.toString(i) + "LetterSuffix_" + wordform.substring(wordform.length() - 1 - i, wordform.length() - 1);
					featureList.add(feature);
				}
			}
		}
		
		if (usePrefixes) {
			// Prefix features
			for (int i = 2; i <= 5; i++) {
				if (wordform.length() > i) {
					feature = Integer.toString(i) + "LetterPrefix_" + wordform.substring(0, i);
					featureList.add(feature);
				}
			}
		}
		
		if (useVowelConsonantPattern) {
			String pattern = "";
			pattern = wordform.replaceAll("[BCDFGHJKLMNPQRSTVWXYZbcdfghjklmnpqrstvwxyz]", "C");
			pattern = pattern.replaceAll("[AEIOUaeiou]", "V");
			feature = "CV-pattern_" + pattern;
			//System.out.println(wordform + " " + pattern);
			featureList.add(feature);
		}
		
		
		
		// context features
		Token tokenBefore = new Token();
		Token tokenAfter = new Token();
		String wordformBefore = null;
		String wordformAfter = null;
		
		if (tokenPosition > 0) {
			tokenBefore = sentence.getToken(tokenPosition - 1);
			wordformBefore = tokenBefore.getWordform().toLowerCase();
		}
		if (tokenPosition < sentence.getNumTokens() - 1) {
			tokenAfter = sentence.getToken(tokenPosition + 1);
			wordformAfter = tokenAfter.getWordform().toLowerCase();
		}
		
		if (useWordformBefore) {
			// wordform of the token before the token
			feature = "wordform-_" + wordformBefore; 
			featureList.add(feature);
		}
		
		if (useWordformAfter) {
			// wordform of the token after the token
			feature = "wordform+_" + wordformAfter; 
			featureList.add(feature);
		}
		
		// Map features to vector positions
		for (String f: featureList) {
			index = getFeatureIndex(f, isTrained);
			if (index != null) {
				featureVector.add(index);
			}
		}
		
		
		// feature vector is not sorted
		return featureVector;
	}
	
	
	/*
	 * method returns the index of the feature and add it to the feature map if necessary. 
	 */
	protected Integer getFeatureIndex(String feature, boolean isTrained) {
		if (isTrained) {
			if (!featureMap.containsKey(feature)) {
				featureMap.put(feature, featureMap.size());
			} 
		}
		
		return featureMap.get(feature);
		
	}
	
	
	/*
	 * extracts feature vectors for all tokens in the given the data object and
	 * save the feature vectors to each of the tokens
	 * (if the boolean parameter isTrained is set to true (during training) unseen features
	 * will get a new id, whereas when it is set to false (during classification) no unseen 
	 * features are added to the feature mapping.)
	 * returns the new data object with feature vectors added to the tokens
	 */
	public Data extractFeatures(Data data, boolean isTrained) {
		Token token;
		Sentence sentence;
		ArrayList<Integer> featureVector;
		
		// iterate over data
		for (int i = 0; i < data.getNumSentences(); i++) {
			sentence = data.getSentence(i);
			
			for (int j=0; j < sentence.getNumTokens(); j++) {
				token = sentence.getToken(j);
				
				// add label to label map
				addLabel(token.getGoldTag());
		
				// compute feature vector for this token
				featureVector = getFeatureVector(j, token, sentence, isTrained);
				// update token with feature vector
				token.setFeatureVector(featureVector);
				data.updateToken(i, j, token);
			}
		}

		return data;
	}
	
	
	/*
	 * saves the current label and feature mapping to the specified file.
	 * Format:	label name + \t + label id
	 * 			feature name + \t + feature id
	 * 			(empty line separating label and feature mapping)
	 */
	public void saveFeatureMapping(String fileName) {
		
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
			
			// save label mappings
			String line;
			for (Map.Entry<String, Integer> labelEntry: labelMap.entrySet()) {
				line = labelEntry.getKey() + "\t" + Integer.toString(labelEntry.getValue());
				writer.write(line + "\n");
			}
			
			// empty line divides label mappings from feature mappings
			for (Map.Entry<String, Integer> featureEntry: featureMap.entrySet()) {
				line = featureEntry.getKey() + "\t" + Integer.toString(featureEntry.getValue());
				writer.write(line + "\n");
			}
			
		} catch (IOException e) {
			// TODO Automatisch generierter Erfassungsblock
			e.printStackTrace();
		}
		
	}

	
	/*
	 * reads a previously saved label and feature mapping from the specified file
	 */
	public void loadFeatureMapping(String fileName) {
		
		try {
			BufferedReader reader = new BufferedReader(new FileReader(fileName));
			
			String line;
			String key;
			Integer value;
			boolean isLabel = true;
			while ((line = reader.readLine()) != null) {
				line = line.trim();
				
				// empty line between label mapping and feature mapping
				if (line.equals("")) {
					isLabel = false;
					continue;
				}
				
				key = line.split("\t")[0];
				value = Integer.parseInt(line.split("\t")[1]);
				
				// read in label
				if (isLabel) {
					addLabel(key, value);
				// read in feature
				} else {
					addFeature(key, value);
				}
			}
			
			
		} catch (FileNotFoundException e) {
			// TODO Automatisch generierter Erfassungsblock
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Automatisch generierter Erfassungsblock
			e.printStackTrace();
		}
	}
}
