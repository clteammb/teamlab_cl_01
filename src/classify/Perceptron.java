package classify;
import java.util.ArrayList;
import java.util.Map;

import dataClasses.Data;
import dataClasses.Token;


public class Perceptron {
	// Weight vector
	private double[] weightVector;
	private int labelID;
	
	Perceptron(int id, int numFeatures) {
		labelID = id;
		weightVector = new double[numFeatures];
	}
	
	Perceptron(int id, double[] weightVector) {
		labelID = id;
		this.weightVector = weightVector;
	}
	
	/*
	 *  Getters
	 */
	public int getLabelID() {
		return labelID;
	}
	
	public double[] getWeightVector() {
		return weightVector;
	}
	
	
	/*
	 * returns the score predicted for the given feature vector
	 */
	double predict(ArrayList<Integer> featureVector) {
		double score = 0.0;
		
		for (Integer i: featureVector) {
			score += weightVector[i];
		}
		
		return score;
	}
	
	/*
	 * update the score for the actually correct label and the given feature vector
	 */
	void increaseWeights(ArrayList<Integer> featureVector) {
		for (Integer i: featureVector) {
			weightVector[i] = weightVector[i] + 0.1;
		}
	}
	
	/*
	 * update the score for the wrongly predicted label and the given feature vector
	 */
	void decreaseWeights(ArrayList<Integer> featureVector) {
		for (Integer i: featureVector) {
			weightVector[i] = weightVector[i] - 0.1;
		}
	}

}
