package classify;

import java.util.ArrayList;
import java.util.HashMap;

import dataClasses.Data;
import dataClasses.Sentence;
import dataClasses.Token;

public class FeatureExtractionGenes extends FeatureExtraction {
	
	public FeatureExtractionGenes(String featureLabelFile) {
		featureMap = new HashMap<String, Integer>();
		labelMap = new HashMap<String, Integer>();
		this.loadFeatureMapping(featureLabelFile);
	}

	public FeatureExtractionGenes() {
		featureMap = new HashMap<String, Integer>();
		labelMap = new HashMap<String, Integer>();
	}

	/*
	 * method creates the feature vector for the token given and its context within the sentence
	 */
	private ArrayList<Integer> getFeatureVector(int tokenPosition, Token token, Sentence sentence, boolean isTrained) {
		String feature;
		String wordform;
		Integer index;
		ArrayList<String> featureList = new ArrayList<String>();
		ArrayList<Integer> featureVector = new ArrayList<Integer>();
				
		// switches for the different features
		boolean useWordform = true;
		
		boolean useWordformBefore = true;
		boolean useWordformAfter = true;
		
		boolean useSuffixes = true;
		boolean usePrefixes = true;
		
		// test only one or both
		boolean useShape = false;
		boolean useCollapsedShape = true;
		
		boolean useVowelConsonantPattern = true;	// could help in identifying foreign words
		
		boolean useRomanNumber = false; 				// TODO: change it to real roman numbers?
		
		// test only one and all three
		boolean useStartsWithCapital = false;
		boolean useCapitalLetterAndMidSentence = false;
		boolean useContainsUpperCase = false;
		
		// test only one and both
		boolean useAllUpperCase = false;
		boolean useAllUpperCaseAndNumber = false;
		
		boolean useContainsNumber = false;
		boolean useContainsHyphen = false;
		boolean useContainsHyphenAndNumber = false;
		boolean useContainsPeriod = false;
		
		
		boolean useContextFeatures = false;
		
		// add all of the features for the previous and next token
		
		
		wordform = token.getWordform();
		
		feature = "defaultFeature";
		featureList.add(feature);
		
		// wordform of the token
		if (useWordform) {
			feature = "wordform_" + wordform.toLowerCase();
			featureList.add(feature);
		}
		
		
		// Starts with capital letter
		if (useStartsWithCapital) {
			if(Character.isUpperCase(wordform.charAt(0))){
				feature = "wordform_StartsWithCapital";
				featureList.add(feature);
			}
		}
		
		if (useCapitalLetterAndMidSentence) {
			// Starts with capital letter and is not at the start of the sentence
			if (tokenPosition == 0 && Character.isUpperCase(wordform.charAt(0))) {
				feature = "CapitalLetterAndMidSentence";
				featureList.add(feature);
			}
		}
		
		if (useContainsUpperCase) {
			// Contains uppercase character
			if (wordform.matches(".*[A-Z].*")) {
				feature = "ContainsUpperCase";
				featureList.add(feature);
			}
		}
		
		if (useContainsNumber) {
			// Contains number
			if (wordform.matches("\\d")) {
				feature = "ContainsNumber";
				featureList.add(feature);
			}
		}
		
		if (useContainsHyphen) {
			// Contains hyphen
			if (wordform.contains("-")) {
				feature = "ContainsHyphen";
				featureList.add(feature);
			}
		}
		
		if (useContainsPeriod) {
			// contains period
			if (wordform.contains(".") && !wordform.equals(".")) {
				feature = "ContainsPeriod";
				featureList.add(feature);
			}
		}
		
		if (useContainsHyphenAndNumber) {
			if (wordform.matches(".*[0-9]+.*") && wordform.contains("-")) {
				feature = "ContainsHyphenAndNumber";
				featureList.add(feature);
			}
		}
		
		if (useAllUpperCaseAndNumber) {
			if(wordform.matches("[A-Z0-9]+")){
				feature = "AllUpperCaseAndNumber";
				featureList.add(feature);
			}
		}
		
		if (useRomanNumber) {
			if(wordform.matches(".*[IVLXDCM]+")){
				feature = "RomanNumber";
				featureList.add(feature);
			}
		}
		
		if (useShape) {
			String shape = wordform;
			shape = shape.replaceAll("[A-Z]", "A");
			shape = shape.replaceAll("[a-z]", "a");
			shape = shape.replaceAll("[0-9]", "0");
			shape = shape.replaceAll("[^Aa0]", "_");
			feature = "Shape=" + shape;
			featureList.add(feature);
		}
		
		if (useCollapsedShape) {
			String collapsedShape = wordform;
			collapsedShape = collapsedShape.replaceAll("[A-Z]", "A");
			collapsedShape = collapsedShape.replaceAll("[a-z]", "a");
			collapsedShape = collapsedShape.replaceAll("[0-9]", "0");
			collapsedShape = collapsedShape.replaceAll("[^Aa0]", "_");
			collapsedShape = collapsedShape.replaceAll("AAA+", "AAA");
			collapsedShape = collapsedShape.replaceAll("aaa+", "aaa");
			collapsedShape = collapsedShape.replaceAll("000+", "000");
			collapsedShape = collapsedShape.replaceAll("___+", "___");
			feature = "CollapsedShape=" + collapsedShape;
			featureList.add(feature);
		}
		
		if (useAllUpperCase) {
			// All letters uppercase (may contain dots)
			if (wordform.matches("([A-Z]*\\.)*[A-Z]+([A-Z]*\\.)*")) {
			//if (wordform.matches("\\b([A-Z]+\\.?)+\\b")) {		--> forgets the period at the end
				feature = "AllUpperCase";
				//System.out.println(wordform);
				featureList.add(feature);
			}
		}
		
		if (useSuffixes) {
			// Suffix features
			for (int i = 2; i <= 5; i++) {
				if (wordform.length() > i) {
					feature = Integer.toString(i) + "LetterSuffix_" + wordform.substring(wordform.length() - 1 - i, wordform.length() - 1);
					featureList.add(feature);
				}
			}
		}
		
		if (usePrefixes) {
			// Prefix features
			for (int i = 2; i <= 5; i++) {
				if (wordform.length() > i) {
					feature = Integer.toString(i) + "LetterPrefix_" + wordform.substring(0, i);
					featureList.add(feature);
				}
			}
		}
		
		if (useVowelConsonantPattern) {
			String pattern = "";
			pattern = wordform.replaceAll("[BCDFGHJKLMNPQRSTVWXYZbcdfghjklmnpqrstvwxyz]", "C");
			pattern = pattern.replaceAll("[AEIOUaeiou]", "V");
			feature = "CV-pattern_" + pattern;
			//System.out.println(wordform + " " + pattern);
			featureList.add(feature);
		}
		
		
		
		// context features
		Token tokenBefore = new Token();
		Token tokenAfter = new Token();
		String wordformBefore = null;
		String wordformAfter = null;
		
		if (tokenPosition > 0) {
			tokenBefore = sentence.getToken(tokenPosition - 1);
			wordformBefore = tokenBefore.getWordform().toLowerCase();
		}
		if (tokenPosition < sentence.getNumTokens() - 1) {
			tokenAfter = sentence.getToken(tokenPosition + 1);
			wordformAfter = tokenAfter.getWordform().toLowerCase();
		}
		
		if (useWordformBefore) {
			// wordform of the token before the token
			feature = "wordform-_" + wordformBefore; 
			featureList.add(feature);
		}
		
		if (useWordformAfter) {
			// wordform of the token after the token
			feature = "wordform+_" + wordformAfter; 
			featureList.add(feature);
		}
		
		if (useContextFeatures) {
			// add features for the previous token
			if (wordformBefore != null) {
				// Starts with capital letter
				if (useStartsWithCapital) {
					if(Character.isUpperCase(wordformBefore.charAt(0))){
						feature = "wordformBefore_StartsWithCapital";
						featureList.add(feature);
					}
				}
				
				if (useCapitalLetterAndMidSentence) {
					// Starts with capital letter and is not at the start of the sentence
					if (tokenPosition == 0 && Character.isUpperCase(wordformBefore.charAt(0))) {
						feature = "WordformBefore_CapitalLetterAndMidSentence";
						featureList.add(feature);
					}
				}
				
				if (useContainsUpperCase) {
					// Contains uppercase character
					if (wordformBefore.matches(".*[A-Z].*")) {
						feature = "WordformBefore_ContainsUpperCase";
						featureList.add(feature);
					}
				}
				
				if (useContainsNumber) {
					// Contains number
					if (wordformBefore.matches("\\d")) {
						feature = "WordformBefore_ContainsNumber";
						featureList.add(feature);
					}
				}
				
				if (useContainsHyphen) {
					// Contains hyphen
					if (wordformBefore.contains("-")) {
						feature = "WordformBefore_ContainsHyphen";
						featureList.add(feature);
					}
				}
				
				if (useContainsPeriod) {
					// contains period
					if (wordformBefore.contains(".") && !wordformBefore.equals(".")) {
						feature = "WordformBefore_ContainsPeriod";
						featureList.add(feature);
					}
				}
				
				if (useContainsHyphenAndNumber) {
					if (wordformBefore.matches(".*[0-9]+.*") && wordformBefore.contains("-")) {
						feature = "WordformBefore_ContainsHyphenAndNumber";
						featureList.add(feature);
					}
				}
				
				if (useAllUpperCaseAndNumber) {
					if(wordformBefore.matches("[A-Z0-9]+")){
						feature = "WordformBefore_AllUpperCaseAndNumber";
						featureList.add(feature);
					}
				}
				
				if (useRomanNumber) {
					if(wordformBefore.matches(".*[IVLXDCM]+")){
						feature = "WordformBefore_RomanNumber";
						System.out.println(wordformBefore);
						featureList.add(feature);
					}
				}
				
				if (useShape) {
					String shape = wordformBefore;
					shape = shape.replaceAll("[A-Z]", "A");
					shape = shape.replaceAll("[a-z]", "a");
					shape = shape.replaceAll("[0-9]", "0");
					shape = shape.replaceAll("[^Aa0]", "_");
					feature = "WordformBefore_Shape=" + shape;
					featureList.add(feature);
				}
				
				if (useCollapsedShape) {
					String collapsedShape = wordformBefore;
					collapsedShape = collapsedShape.replaceAll("[A-Z]", "A");
					collapsedShape = collapsedShape.replaceAll("[a-z]", "a");
					collapsedShape = collapsedShape.replaceAll("[0-9]", "0");
					collapsedShape = collapsedShape.replaceAll("[^Aa0]", "_");
					collapsedShape = collapsedShape.replaceAll("AAA+", "AAA");
					collapsedShape = collapsedShape.replaceAll("aaa+", "aaa");
					collapsedShape = collapsedShape.replaceAll("000+", "000");
					collapsedShape = collapsedShape.replaceAll("___+", "___");
					feature = "WordformBefore_CollapsedShape=" + collapsedShape;
					featureList.add(feature);
				}
				
				if (useAllUpperCase) {
					// All letters uppercase (may contain dots)
					if (wordformBefore.matches("([A-Z]*\\.)*[A-Z]+([A-Z]*\\.)*")) {
					//if (wordformBefore.matches("\\b([A-Z]+\\.?)+\\b")) {		--> forgets the period at the end
						feature = "WordformBefore_AllUpperCase";
						//System.out.println(wordformBefore);
						featureList.add(feature);
					}
				}
				
				if (useSuffixes) {
					// Suffix features
					for (int i = 2; i <= 5; i++) {
						if (wordformBefore.length() > i) {
							feature = Integer.toString(i) + "WordformBefore_LetterSuffix_" + wordformBefore.substring(wordformBefore.length() - 1 - i, wordformBefore.length() - 1);
							featureList.add(feature);
						}
					}
				}
				
				if (usePrefixes) {
					// Prefix features
					for (int i = 2; i <= 5; i++) {
						if (wordformBefore.length() > i) {
							feature = Integer.toString(i) + "WordformBefore_LetterPrefix_" + wordformBefore.substring(0, i);
							featureList.add(feature);
						}
					}
				}
				
				if (useVowelConsonantPattern) {
					String pattern = "";
					pattern = wordformBefore.replaceAll("[BCDFGHJKLMNPQRSTVWXYZbcdfghjklmnpqrstvwxyz]", "C");
					pattern = pattern.replaceAll("[AEIOUaeiou]", "V");
					feature = "WordformBefore_CV-pattern_" + pattern;
					//System.out.println(wordformBefore + " " + pattern);
					featureList.add(feature);
				}
				
				
			}
			
			// add features for the next token
			if (wordformAfter != null) {
				// Starts with capital letter
				if (useStartsWithCapital) {
					if(Character.isUpperCase(wordformAfter.charAt(0))){
						feature = "wordformAfter_StartsWithCapital";
						featureList.add(feature);
					}
				}
				
				if (useCapitalLetterAndMidSentence) {
					// Starts with capital letter and is not at the start of the sentence
					if (tokenPosition == 0 && Character.isUpperCase(wordformAfter.charAt(0))) {
						feature = "wordformAfter_CapitalLetterAndMidSentence";
						featureList.add(feature);
					}
				}
				
				if (useContainsUpperCase) {
					// Contains uppercase character
					if (wordformAfter.matches(".*[A-Z].*")) {
						feature = "wordformAfter_ContainsUpperCase";
						featureList.add(feature);
					}
				}
				
				if (useContainsNumber) {
					// Contains number
					if (wordformAfter.matches("\\d")) {
						feature = "wordformAfter_ContainsNumber";
						featureList.add(feature);
					}
				}
				
				if (useContainsHyphen) {
					// Contains hyphen
					if (wordformAfter.contains("-")) {
						feature = "wordformAfter_ContainsHyphen";
						featureList.add(feature);
					}
				}
				
				if (useContainsPeriod) {
					// contains period
					if (wordformAfter.contains(".") && !wordformAfter.equals(".")) {
						feature = "wordformAfter_ContainsPeriod";
						featureList.add(feature);
					}
				}
				
				if (useContainsHyphenAndNumber) {
					if (wordformAfter.matches(".*[0-9]+.*") && wordformAfter.contains("-")) {
						feature = "wordformAfter_ContainsHyphenAndNumber";
						featureList.add(feature);
					}
				}
				
				if (useAllUpperCaseAndNumber) {
					if(wordformAfter.matches("[A-Z0-9]+")){
						feature = "wordformAfter_AllUpperCaseAndNumber";
						featureList.add(feature);
					}
				}
				
				if (useRomanNumber) {
					if(wordformAfter.matches(".*[IVLXDCM]+")){
						feature = "wordformAfter_RomanNumber";
						System.out.println(wordformAfter);
						featureList.add(feature);
					}
				}
				
				if (useShape) {
					String shape = wordformAfter;
					shape = shape.replaceAll("[A-Z]", "A");
					shape = shape.replaceAll("[a-z]", "a");
					shape = shape.replaceAll("[0-9]", "0");
					shape = shape.replaceAll("[^Aa0]", "_");
					feature = "wordformAfter_Shape=" + shape;
					featureList.add(feature);
				}
				
				if (useCollapsedShape) {
					String collapsedShape = wordformAfter;
					collapsedShape = collapsedShape.replaceAll("[A-Z]", "A");
					collapsedShape = collapsedShape.replaceAll("[a-z]", "a");
					collapsedShape = collapsedShape.replaceAll("[0-9]", "0");
					collapsedShape = collapsedShape.replaceAll("[^Aa0]", "_");
					collapsedShape = collapsedShape.replaceAll("AAA+", "AAA");
					collapsedShape = collapsedShape.replaceAll("aaa+", "aaa");
					collapsedShape = collapsedShape.replaceAll("000+", "000");
					collapsedShape = collapsedShape.replaceAll("___+", "___");
					feature = "wordformAfter_CollapsedShape=" + collapsedShape;
					featureList.add(feature);
				}
				
				if (useAllUpperCase) {
					// All letters uppercase (may contain dots)
					if (wordformAfter.matches("([A-Z]*\\.)*[A-Z]+([A-Z]*\\.)*")) {
					//if (wordformAfter.matches("\\b([A-Z]+\\.?)+\\b")) {		--> forgets the period at the end
						feature = "wordformAfter_AllUpperCase";
						//System.out.println(wordformAfter);
						featureList.add(feature);
					}
				}
				
				if (useSuffixes) {
					// Suffix features
					for (int i = 2; i <= 5; i++) {
						if (wordformAfter.length() > i) {
							feature = Integer.toString(i) + "wordformAfter_LetterSuffix_" + wordformAfter.substring(wordformAfter.length() - 1 - i, wordformAfter.length() - 1);
							featureList.add(feature);
						}
					}
				}
				
				if (usePrefixes) {
					// Prefix features
					for (int i = 2; i <= 5; i++) {
						if (wordformAfter.length() > i) {
							feature = Integer.toString(i) + "wordformAfter_LetterPrefix_" + wordformAfter.substring(0, i);
							featureList.add(feature);
						}
					}
				}
				
				if (useVowelConsonantPattern) {
					String pattern = "";
					pattern = wordformAfter.replaceAll("[BCDFGHJKLMNPQRSTVWXYZbcdfghjklmnpqrstvwxyz]", "C");
					pattern = pattern.replaceAll("[AEIOUaeiou]", "V");
					feature = "wordformAfter_CV-pattern_" + pattern;
					//System.out.println(wordformAfter + " " + pattern);
					featureList.add(feature);
				}
			}
		}
		
		// Map features to vector positions
		for (String f: featureList) {
			index = getFeatureIndex(f, isTrained);
			if (index != null) {
				featureVector.add(index);
			}
		}
		
		
		// feature vector is not sorted
		return featureVector;
	}
	
	
	
	/*
	 * extracts feature vectors for all tokens in the given the data object and
	 * save the feature vectors to each of the tokens
	 * (if the boolean parameter isTrained is set to true (during training) unseen features
	 * will get a new id, whereas when it is set to false (during classification) no unseen 
	 * features are added to the feature mapping.)
	 * returns the new data object with feature vectors added to the tokens
	 */
	public Data extractFeatures(Data data, boolean isTrained) {
		Token token;
		Sentence sentence;
		ArrayList<Integer> featureVector;
		
		// iterate over data
		for (int i = 0; i < data.getNumSentences(); i++) {
			sentence = data.getSentence(i);
			
			for (int j=0; j < sentence.getNumTokens(); j++) {
				token = sentence.getToken(j);
				
				// add label to label map
				addLabel(token.getGoldTag());
		
				// compute feature vector for this token
				featureVector = getFeatureVector(j, token, sentence, isTrained);
				// update token with feature vector
				token.setFeatureVector(featureVector);
				data.updateToken(i, j, token);
			}
		}

		return data;
	}
	



}
