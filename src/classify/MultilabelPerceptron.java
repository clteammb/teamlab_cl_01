package classify;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import dataClasses.Data;
import dataClasses.Sentence;
import dataClasses.Token;

public class MultilabelPerceptron {
	private ArrayList<Perceptron> binaryPerceptrons;
	private FeatureExtraction featureMap;
	private boolean posTagging;
	
	public MultilabelPerceptron(boolean posTagging) {
		this.posTagging = posTagging;
	}
	
	public void trainPerceptron(Data data, int iterations) {
		// compute the feature vectors for the data
		if (posTagging) {
			featureMap = new FeatureExtraction();
		} else {
			featureMap = new FeatureExtractionGenes();
		}
		
		featureMap.extractFeatures(data, true);
		int numFeatures = featureMap.getNumFeatures();
		
		// initialize a binary perceptron for each label
		binaryPerceptrons = new ArrayList<Perceptron>();
		for (int i = 0; i < featureMap.getNumLabels(); i++) {
			binaryPerceptrons.add(new Perceptron(i, numFeatures));
		}
		
		// training iterations
		int predictedLabelID;
		ArrayList<Integer> featureVector;
		ArrayList<Sentence> sentenceList = data.getData();
		Perceptron perceptron;
		for (int i = 0; i < iterations; i++) {
			System.out.println("Iteration " + i);
			// iterate over data
			for (Sentence sentence: sentenceList) {
				for (Token token: sentence.getTokens()) {
					featureVector = token.getFeatureVector();
					// predict tag
					predictedLabelID = classifyToken(featureVector);
					// if incorrectly predicted --> change weights
					if (!token.getGoldTag().equals(featureMap.getLabel(predictedLabelID))) {
						// increase weights of gold label perceptron
						perceptron = binaryPerceptrons.get(featureMap.getLabel(token.getGoldTag()));
						perceptron.increaseWeights(featureVector);
						binaryPerceptrons.remove(featureMap.getLabel(token.getGoldTag()));
						binaryPerceptrons.add(featureMap.getLabel(token.getGoldTag()),perceptron);
						// decrease weights of wrongly predicted label perceptron
						perceptron = binaryPerceptrons.get(predictedLabelID);
						perceptron.decreaseWeights(featureVector);
						binaryPerceptrons.remove(predictedLabelID);
						binaryPerceptrons.add(predictedLabelID,perceptron);
						
						
					}
				}
			}
		}
		
	}
	
	
	
	public void trainRandomizedPerceptron(Data data, int iterations) {
		// compute the feature vectors for the data
		if (posTagging) {
			featureMap = new FeatureExtraction();
		} else {
			featureMap = new FeatureExtractionGenes();
		}
		
		featureMap.extractFeatures(data, true);
		int numFeatures = featureMap.getNumFeatures();
		
		// initialize a binary perceptron for each label
		binaryPerceptrons = new ArrayList<Perceptron>();
		for (int i = 0; i < featureMap.getNumLabels(); i++) {
			binaryPerceptrons.add(new Perceptron(i, numFeatures));
		}
		
		// training iterations
		int predictedLabelID;
		ArrayList<Integer> featureVector;
		ArrayList<Sentence> sentenceList = data.getData();
		Perceptron perceptron;
		for (int i = 0; i < iterations; i++) {
			System.out.println("Iteration " + i);
			// iterate over data (randomized order of sentences)
			Collections.shuffle(sentenceList);
			for (Sentence sentence: sentenceList) {
				// randomize order of tokens in sentence
				ArrayList<Token> tokenList = sentence.getTokens();
				Collections.shuffle(tokenList);
				for (Token token: tokenList) {
					featureVector = token.getFeatureVector();
					// predict tag
					predictedLabelID = classifyToken(featureVector);
					// if incorrectly predicted --> change weights
					if (!token.getGoldTag().equals(featureMap.getLabel(predictedLabelID))) {
						// increase weights of gold label perceptron
						perceptron = binaryPerceptrons.get(featureMap.getLabel(token.getGoldTag()));
						perceptron.increaseWeights(featureVector);
						binaryPerceptrons.remove(featureMap.getLabel(token.getGoldTag()));
						binaryPerceptrons.add(featureMap.getLabel(token.getGoldTag()),perceptron);
						// decrease weights of wrongly predicted label perceptron
						perceptron = binaryPerceptrons.get(predictedLabelID);
						perceptron.decreaseWeights(featureVector);
						binaryPerceptrons.remove(predictedLabelID);
						binaryPerceptrons.add(predictedLabelID,perceptron);
						
						
					}
				}
			}
		}
		
	}
	
	
	public Data classifyData(Data data) throws Exception {
		
		if (featureMap == null || binaryPerceptrons == null) {
			throw new Exception("No model specified!");
		}
		
		featureMap.extractFeatures(data, false);
		
		ArrayList<Sentence> sentenceList = data.getData();
		ArrayList<Token> tokenList;
		int predictedLabelID;
		String predictedLabel;
		Token token;
		// iterate over each sentence
		for (int i = 0; i < sentenceList.size(); i++) {
			tokenList = sentenceList.get(i).getTokens();
			
			// iterate over each token
			for(int j = 0 ; j < tokenList.size() ; j++){
				token = tokenList.get(j);
				// classify token
				predictedLabelID = classifyToken(token.getFeatureVector());
				predictedLabel = featureMap.getLabel(predictedLabelID);
				token.setPredTag(predictedLabel);
				// update data with the token containing the predicted label
				data.updateToken(i, j, token);
			}
			
			
		}
		
		return data;
	}

	
	private int classifyToken(ArrayList<Integer> featureVector) {
		double score;
		int labelID = 0;
		double highestScore = binaryPerceptrons.get(0).predict(featureVector);
		
		for (int i = 1; i < binaryPerceptrons.size(); i++) {
			score = binaryPerceptrons.get(i).predict(featureVector);
			if (score > highestScore) {
				highestScore = score;
				labelID = i;
			}
		}
		
		return labelID;
	}
	
	// save feature and label mapping and model to the specified files
	public void saveModel(String featureFile, String modelFile) {
		
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(modelFile));
			
			// save features mappings and label mappings
			featureMap.saveFeatureMapping(featureFile);
			
			// save model (= weight vectors of perceptrons + labels)
			int label;
			double[] weightVector;
			String line;
			for (Perceptron perceptron: binaryPerceptrons) {
				label = perceptron.getLabelID();
				weightVector = perceptron.getWeightVector();
				
				// concatenate label and weight vector
				line = Integer.toString(label);
				//for (double digit: weightVector) {
				for(int i=0; i<weightVector.length;i++){
					if(weightVector[i] != 0.0) {
						line += "\t" + Integer.toString(i) + ":" + String.valueOf(weightVector[i]);
					}
				}
				// save label and weight vector of perceptron to model file
				writer.write(line + "\n");
			}
			
		} catch (IOException e) {
			// TODO Automatisch generierter Erfassungsblock
			e.printStackTrace();
		}
	}
	
	// load feature and label mapping and model from the specified files
	public void loadModel(String featureFile, String modelFile) {
		
		try {
			BufferedReader reader = new BufferedReader(new FileReader(modelFile));
			
			// load feature and label mapping
			if (posTagging) {
				this.featureMap = new FeatureExtraction(featureFile);
			} else {
				this.featureMap = new FeatureExtractionGenes(featureFile);
			}
			
			// load model
			String line;
			String[] splittedLine;
			int id;
			double[] weightVector;
			String[] indexWeight;
			this.binaryPerceptrons = new ArrayList<Perceptron>();
			
			while ((line = reader.readLine()) != null) {
				splittedLine = line.trim().split("\t");
				
				id = Integer.parseInt(splittedLine[0]);
				// read weight Vector
				weightVector = new double[splittedLine.length - 1];
				for (int i = 1; i < splittedLine.length - 1; i++) {
					indexWeight = splittedLine[i].split(":");
					weightVector[Integer.parseInt(indexWeight[0])] = Double.parseDouble(indexWeight[1]);
				}
				// add perceptron to binary perceptrons list
				this.binaryPerceptrons.add(new Perceptron(id, weightVector));
				
			}
		} catch (FileNotFoundException e) {
			// TODO Automatisch generierter Erfassungsblock
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Automatisch generierter Erfassungsblock
			e.printStackTrace();
		}
		
		
		
	}
}
