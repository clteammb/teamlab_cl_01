package classify;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import dataClasses.Data;
import dataClasses.Sentence;
import dataClasses.Token;

public class DTNode implements Serializable {
	
	private DTNode leftChild; // feature holds for this child
	private DTNode rightChild; // feature does not hold for this child
	private List<Token> data;
	public int featureID;
	public String label;
	private ArrayList<Integer> usedFeatures;
	private int numFeatures;
	private double entropy;
	private double informationGainThreshold = 0.0;
	private int maxTokensLeaf = 1;
	private String splittingCriteria;
	
	DTNode(Data data, ArrayList<Integer> usedFeatures, int numFeatures, String splittingCriteria) {
		this.data = new ArrayList<Token>();
		for (Sentence sentence: data.getData()) {
			for (Token token: sentence.getTokens()) {
				this.data.add(token);
			}
		}
		this.usedFeatures = usedFeatures;
		this.numFeatures = numFeatures;
		if (splittingCriteria.equals("MI")) {
			this.splittingCriteria = "MI";
		} else {
			this.splittingCriteria = "IG";
		}
	}
	
	DTNode(ArrayList<Token> data, ArrayList<Integer> usedFeatures, int numFeatures, String splittingCriteria) {
		this.data = data;
		this.usedFeatures = usedFeatures;
		this.numFeatures = numFeatures;
		if (splittingCriteria.equals("MI")) {
			this.splittingCriteria = "MI";
		} else {
			this.splittingCriteria = "IG";
		}
	}
	
	void setInformationGainThreshold(double threshold) {
		this.informationGainThreshold = threshold;
	}
	
	void setMaxTokensLeaf(int num) {
		this.maxTokensLeaf = num;
	}
	
	String classifyToken(ArrayList<Integer> featureVector) {
		// Leaf Node: return label
		if (featureID == -1) {
			return label;
		}

		// continue with left or right child node depending on whether the feature applies or not
		if (featureVector.contains(featureID)) {
			return leftChild.classifyToken(featureVector);
		} else {
			return rightChild.classifyToken(featureVector);
		}
	}
	
	void divideData() {
		System.out.println("Tokens: " + data.size());
		
		HashMap<String, Integer> labelCount = getLabelCount();
		
		// Data at this node contains only one type of label --> make leaf node
		if (labelCount.keySet().size() == 1 || this.data.size() < this.maxTokensLeaf) {
		//if (labelCount.keySet().size() == 1) {
			this.featureID = -1;
			this.label = getMajorityLabel(labelCount);
			// end processing of function
			return;
		}
		
		// find best feature to divide the data
		int divisionCriteria = chooseDivisionCriteria();
		// No (good) feature left for dividing the data --> make leaf node
		if (divisionCriteria == -1) {
			this.featureID = -1;
			this.label = getMajorityLabel(labelCount);
			// end processing of function
			return;
		}
		
		// the current node is not a leaf --> divide data further
		this.featureID = divisionCriteria;
		this.usedFeatures.add(divisionCriteria);
		
		// divide data into to subsets according to this features value
		ArrayList<Token> featureHolds = new ArrayList<Token>();
		ArrayList<Token> featureHoldsNot = new ArrayList<Token>();
		for (Token token: this.data) {
			if (token.getFeatureVector().contains(divisionCriteria)) {
				featureHolds.add(token);
			} else {
				featureHoldsNot.add(token);
			}
		}
		
		System.out.println("Left child: " + featureHolds.size() + "\tRight child: " + featureHoldsNot.size());
		
		ArrayList<Integer> featureList1 = new ArrayList<Integer>();
		featureList1.addAll(this.usedFeatures);
		ArrayList<Integer> featureList2 = new ArrayList<Integer>();
		featureList2.addAll(this.usedFeatures);
		// create child nodes
		this.leftChild = new DTNode(featureHolds, featureList1, this.numFeatures, this.splittingCriteria);
		this.rightChild = new DTNode(featureHoldsNot, featureList2, this.numFeatures, this.splittingCriteria);
		
		// set information gain threshold
		this.leftChild.setInformationGainThreshold(this.informationGainThreshold);
		this.rightChild.setInformationGainThreshold(this.informationGainThreshold);
		
		// start decision tree building process for child nodes
		this.leftChild.divideData();
		this.rightChild.divideData();
		
	}
	
	private int chooseDivisionCriteria() {
		int bestFeature = -1;
		double bestScore = 0.0;
		double currentScore;
		
		// calculate entropy of this node if the splitting criteria is information gain
		if (this.splittingCriteria.equals("IG")) {
			this.entropy = getEntropy(this.data);
		}
		
		// iterate over all possible features
		for (int currentFeature = 0; currentFeature < this.numFeatures; currentFeature++) {
			// only look at features that have not been used
			if (!usedFeatures.contains(currentFeature)) {
				
				// calculate score of this feature
				// using mutual information
				if (this.splittingCriteria.equals("MI")) {
					currentScore = getMutualInformation(currentFeature);
				// using information gain
				} else {
					currentScore = getInformationGain(currentFeature);
				}
			
				// found a new best feature
				if (currentScore > bestScore) {
					bestFeature = currentFeature;
					bestScore = currentScore;
				}
			}
		}
		
		return bestFeature;
	}
	
	
//	private double getScore(int feature) {
//		double score = Math.random() + 0.1;
//	
//		// divide the tokens by this features value
//		ArrayList<Token> featureHolds = new ArrayList<Token>();
//		ArrayList<Token> featureHoldsNot = new ArrayList<Token>();
//		for (Token token: this.data) {
//			if (token.getFeatureVector().contains(feature)) {
//				featureHolds.add(token);
//			} else {
//				featureHoldsNot.add(token);
//			}
//		}
//		
//		// if all the data items fulfill this feature or do not fulfill it, it is a bad feature
//		if (featureHolds.size() == 0 || featureHoldsNot.size() == 0) {
//			score = 0.0;
//			usedFeatures.add(feature);
//		}
//		
//		return score;
//	}
	
	private double getMutualInformation(int feature) {
		/*
		 * Mutual Information
		 */
		// keep track of feature-label distribution: 
		Map<String, Integer> featureAppliesCount = new HashMap<String, Integer>();
		Map<String, Integer> featureAppliesNotCount = new HashMap<String, Integer>();
		String lab;
		
		for (Token token: data) {
			lab = token.getGoldTag();
			if (token.getFeatureVector().contains(feature)) {
				if(!featureAppliesCount.containsKey(lab)) {
					featureAppliesCount.put(lab, 0);
				}
				featureAppliesCount.put(lab, featureAppliesCount.get(lab) + 1);
			} else {
				if(!featureAppliesNotCount.containsKey(lab)) {
					featureAppliesNotCount.put(lab, 0);
				}
				featureAppliesNotCount.put(lab, featureAppliesNotCount.get(lab) + 1);
			}
		}
		
		// iterate over all possible labels
		ArrayList<String> labelList = new ArrayList<String>();
		Set<String> labels = new HashSet<String>();
		labels.addAll(featureAppliesCount.keySet());
		labels.addAll(featureAppliesNotCount.keySet());
		labelList.addAll(labels);
		double probL1, probL0, probL, prob1, prob0;
		int count1, count0;
		double mutualInformation = 0.0;

		// Probability that the feature applies - P(1)
		count1 = 0;
		for (int value: featureAppliesCount.values()) {
			count1 += value;
		}
		prob1 = count1 / (double) data.size();
		
		// Probability that the feature does not apply - P(0)
		count0 = 0;
		for (int value: featureAppliesNotCount.values()) {
			count0 += value;
		}
		prob0 = count0 / (double) data.size();
		
		//System.out.println("P(1): " + prob1 + " , P(0): " + prob0);
		
		for (String l: labelList) {
			// Probability of the label when the feature applies - P(L, 1)
			probL1 = featureAppliesCount.getOrDefault(l, 0) / (double) data.size();
			// Probability of the label when the feature does not apply - P(L, 0)
			probL0 = featureAppliesNotCount.getOrDefault(l, 0) / (double) data.size();
			// Probability of the label - P(L)
			probL = (featureAppliesCount.getOrDefault(l, 0) 
					+ featureAppliesNotCount.getOrDefault(l, 0))
					/ (double) data.size();
			
			//System.out.println("P(L, 1): " + probL1 + " , P(L, 0): " + probL0 + " , P(L): " + probL);
			if (probL1 != 0.0 && prob1 != 0 && probL != 0) {
				mutualInformation += probL1 * Math.log(probL1 / (probL * prob1));
			}
			if (probL0 != 0.0 && prob0 != 0 && probL != 0) {
				mutualInformation += probL0 * Math.log(probL0 / (probL * prob0));
			}
			//System.out.println("MutualInformation: " + mutualInformation);
			
		}
		
		if (mutualInformation == 0.0) {
			usedFeatures.add(feature);
		}
		
		return mutualInformation;
	}
	
	private double getInformationGain(int feature) {
			
		// split data according to feature
		ArrayList<Token> featureHolds = new ArrayList<Token>();
		ArrayList<Token> featureHoldsNot = new ArrayList<Token>();
		
		for (Token token: this.data) {
			if (token.getFeatureVector().contains(feature)) {
				featureHolds.add(token);
			} else {
				featureHoldsNot.add(token);
			}
		}
		
		double entropyChild1 = getEntropy(featureHolds);
		double entropyChild2 = getEntropy(featureHoldsNot);
		int sizeChild1 = featureHolds.size();
		int sizeChild2 = featureHoldsNot.size();
		int parentSize = this.data.size();
		
		double informationGain = this.entropy
							   	 - (sizeChild1 / (double) parentSize) * entropyChild1
							   	 - (sizeChild2 / (double) parentSize) * entropyChild2;
		
		// information gain is zero --> feature is useless
		if (informationGain == 0.0) {
			this.usedFeatures.add(feature);
		}
		
		// do not use feature if information gain is below a certain threshold
		if (informationGain < this.informationGainThreshold) {
			informationGain = 0.0;
		}
		
		return informationGain;
	}
	
	/*
	 * Calculates entropy: 
	 * 		Entropy = sum(-p(L) log p(L))
	 */
	private double getEntropy(List<Token> d) {
		String lab;
		
		// count labels in data
		HashMap<String, Integer> labelCount = new HashMap<String, Integer>();
		for (Token token: d) {
			lab = token.getGoldTag();
			if (!labelCount.containsKey(lab)) {
				labelCount.put(lab, 1);
			} else {
				labelCount.put(lab, labelCount.get(lab) + 1);
			}
		}
		
		double probL;
		double entropy = 0.0;
		// iterate over all possible labels in the data
		for (String l: labelCount.keySet()) {
			probL = labelCount.get(l) / (double) d.size();
			
			if (probL != 0.0) {
				entropy -= probL * Math.log(probL);
			}
		}
		// p(L) * log p(L)
		
		return entropy;
	}
	
	private HashMap<String, Integer> getLabelCount() {
		HashMap<String, Integer> labelCount = new HashMap<String, Integer>();
		String l;
		for (Token token: data) {
			l = token.getGoldTag();
			if (!labelCount.containsKey(l)) {
				labelCount.put(l, 1);
			} else {
				labelCount.put(l, labelCount.get(l) + 1);
			}
		}
		return labelCount;
	}
	
	private String getMajorityLabel(HashMap<String, Integer> labelCount) {
		String bestLabel = null;
		int count = 0;
		for (String l: labelCount.keySet()) {
			if (labelCount.get(l) > count) {
				count = labelCount.get(l);
				bestLabel = l;
			}
		}
		return bestLabel;
	}
}
