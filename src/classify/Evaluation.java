package classify;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;


public class Evaluation {
	// Precision, Recall and F-Score for each of the classes
	private Map<String, Double> precisionForLabels;
	private Map<String, Double> recallForLabels;
	private Map<String, Double> fScoreForLabels;
	// Accuracy and macro-averaged precision, recall and F-Score
	private double macroPrecision;
	private double macroRecall;
	private double macroFScore;
	private double accuracy;
	
	NumberFormat formatter = new DecimalFormat("#0.00");     
	
	
	/*
	 * Getters and Setters
	 */
	public Map<String, Double> getPrecisionForLabels() {
		return precisionForLabels;
	}
	public void setPrecisionForLabels(Map<String, Double> precisionForLabels) {
		this.precisionForLabels = precisionForLabels;
	}
	public Map<String, Double> getRecallForLabels() {
		return recallForLabels;
	}
	public void setRecallForLabels(Map<String, Double> recallForLabels) {
		this.recallForLabels = recallForLabels;
	}
	public Map<String, Double> getfScoreForLabels() {
		return fScoreForLabels;
	}
	public void setfScoreForLabels(Map<String, Double> fScoreForLabels) {
		this.fScoreForLabels = fScoreForLabels;
	}
	public double getMacroPrecision() {
		return macroPrecision;
	}
	public void setMacroPrecision(double macroPrecision) {
		this.macroPrecision = macroPrecision;
	}
	public double getMacroRecall() {
		return macroRecall;
	}
	public void setMacroRecall(double macroRecall) {
		this.macroRecall = macroRecall;
	}
	public double getMacroFScore() {
		return macroFScore;
	}
	public void setMacroFScore(double macroFScore) {
		this.macroFScore = macroFScore;
	}
	public double getAccuracy() {
		return accuracy;
	}
	public void setAccuracy(double accuracy) {
		this.accuracy = accuracy;
	}
	
	
	public void printEvaluation() {
		String line;
		System.out.println("Precision\tRecall\tFScore");
		ArrayList<String> labelList = new ArrayList<String>(precisionForLabels.keySet());
		Collections.sort(labelList);
		for (String label: labelList) {
			line = String.valueOf(label + "\t" 
							+ formatter.format(precisionForLabels.get(label) * 100)) + "\t" 
							+ formatter.format(recallForLabels.get(label) * 100) + "\t" 
							+ formatter.format(fScoreForLabels.get(label) * 100);
//			line = String.valueOf(label + " & " 
//					+ formatter.format(precisionForLabels.get(label))) + " & " 
//					+ formatter.format(recallForLabels.get(label)) + " & " 
//					+ formatter.format(fScoreForLabels.get(label)) + " \\tabularnewline";
			System.out.println(line);
		}
		System.out.println("Macro-averaged Precision: " + formatter.format(macroPrecision * 100));
		System.out.println("Macro-averaged Recall: " + formatter.format(macroRecall * 100));
		System.out.println("Macro-averaged F-Score: " + formatter.format(macroFScore * 100));
		System.out.println("Accuracy (over all classes): " + formatter.format(accuracy * 100));
	}
	
	
}
