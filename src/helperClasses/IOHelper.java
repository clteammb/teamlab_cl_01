package helperClasses;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;

import classify.DecisionTree;
import dataClasses.Data;
import dataClasses.Sentence;
import dataClasses.Token;

public class IOHelper {
	
	
	/*
	 * reads in data from the gold standard file and predicted file
	 * and returns a Data object
	 */
	public Data readFiles(String goldFile, String predFile) throws IOException {
		Sentence sentence = new Sentence();
		Token token;
		String[] splittedLine;
		Data data = new Data();
		
		
			BufferedReader inGold = new BufferedReader(new FileReader(goldFile));
			BufferedReader inPred = new BufferedReader(new FileReader(predFile));
			String lineGold = null;
			String linePred = null;
			// read line for gold standard
			while ((lineGold = inGold.readLine()) != null) {
				lineGold = lineGold.trim();
				linePred = inPred.readLine();
				if (linePred == null) {
					throw new IOException("Predicted file is shorter than gold file");
				}
				linePred = linePred.trim();
				// end of sentence --> add sentence to data and start new sentence
				if (lineGold.equals("")) {
					data.addSentence(sentence);
					sentence = new Sentence();
				// new token for sentence
				} else {
					// get wordform, gold tag and predicted tag for token
					token = new Token();
					splittedLine = lineGold.split("\t");
					token.setWordform(splittedLine[0]);
					token.setGoldTag(splittedLine[1]);
					
					if (linePred != null) {
						splittedLine = linePred.split("\t");
						//System.out.println(linePred);					
						token.setPredTag(splittedLine[1]);
					}
					// add token to current sentence
					sentence.addToken(token);
				}
				
			}
			
			if (sentence.getNumTokens() > 0) {
				data.addSentence(sentence);
			}
			
			inGold.close();
			inPred.close();
		
		
		return data;
	}
	
	/*
	 * reads a file with one word form per line (empty lines as sentence boundaries)
	 */
	public Data readTokenFile(String fileName) throws IOException {
		Data data = new Data();
		Sentence sentence = new Sentence();
		Token token = new Token();
	
		BufferedReader readToken = new BufferedReader(new FileReader(fileName));
		String line;
		while((line = readToken.readLine()) != null) {
			line = line.trim().split("\t")[0];
			
			// new sentence
			if(line.equals("")){
				data.addSentence(sentence);
				sentence = new Sentence();
			} else {
				token = new Token();
				token.setWordform(line);
				sentence.addToken(token);
			}
		}
		
		if (sentence.getNumTokens() > 0) {
			data.addSentence(sentence);
		}
		
		readToken.close();
		
		return data;
	}
	
	
	public void saveData(String fileName, Data data, boolean saveGoldTag) throws IOException {
		
		BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
		
		for (Sentence sentence: data.getData()) {
			for (Token token: sentence.getTokens()) {
				
				if (saveGoldTag) {
					writer.write(token.getWordform() + "\t" + token.getGoldTag() + "\n");
					//System.out.print(token.getWordform() + "\t" + token.getGoldTag() + "\n");
				} else {
					writer.write(token.getWordform() + "\t" + token.getPredTag() + "\n");
					//System.out.print(token.getWordform() + "\t" + token.getPredTag() + "\n");
				}
			}
			writer.write("\n");
			//System.out.println();
		}
		
		writer.close();
	}
	
	
	public DecisionTree loadDecisionTreeModel(String fileName) throws IOException, ClassNotFoundException {
		FileInputStream file = new FileInputStream("decisionTreeModel.ser");
		BufferedInputStream buffer = new BufferedInputStream(file);
		ObjectInputStream inputStream = new ObjectInputStream(buffer);

		DecisionTree dt = (DecisionTree) inputStream.readObject();

		inputStream.close();
		
		return dt;
	}

}
