import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import classify.Evaluation;
import classify.MultilabelPerceptron;
import helperClasses.IOHelper;
import dataClasses.Data;


public class MainGeneIdentification {

	public static void main(String[] args) {
		// TODO Automatisch generierter Methodenstub
		
//		String trainingDataFile = "/home/users0/klingern/teaching/team-lab-ss2015/data/biomedical/biocreative/train.iob";
//		String testingDataFile = "/home/users0/klingern/teaching/team-lab-ss2015/data/biomedical/biocreative/test.iob";
		String trainingDataFile = "/home/users0/rothmube/teamlab/data/GeneExtraction/80_train.iob";
		String testingDataFile = "/home/users0/rothmube/teamlab/data/GeneExtraction/20_train_development.iob";
		
		String saveTrainingFile = "/home/users0/rothmube/teamlab/data/resultsGene_train.iob"; 
		String saveTestingFile = "/home/users0/rothmube/teamlab/data/resultsGene_development.iob";
//		String saveTrainingFile = "/home/users0/guptamk/teamlab/data/resultsGene_train.iob"; 
//		String saveTestingFile = "/home/users0/guptamk/teamlab/data/resultsGene_test.iob";
		
		IOHelper ioHelper = new IOHelper();
		try {
			Data trainingData = ioHelper.readFiles(trainingDataFile, trainingDataFile);
							
			MultilabelPerceptron mp = new MultilabelPerceptron(false);
			mp.trainPerceptron(trainingData, 10);
			
			mp.saveModel("/home/users0/rothmube/teamlab/models/features-gene.txt", "/home/users0/rothmube/teamlab/models/perceptronModel-gene.txt");
			
			//trainingData = ioHelper.readTokenFile(trainingDataFile);
			Data testingData = ioHelper.readTokenFile(testingDataFile);
			trainingData = mp.classifyData(trainingData);
			testingData = mp.classifyData(testingData);

			//ioHelper.saveData(saveTrainingFile, trainingData, false);
			ioHelper.saveData(saveTestingFile, testingData, false);
			
		} catch (IOException e) {
			// TODO Automatisch generierter Erfassungsblock
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Automatisch generierter Erfassungsblock
			e.printStackTrace();
		}
		
		Data evaluateDataTrain;
		try {
//			System.out.println("Training Set: ");
//			evaluateDataTrain = ioHelper.readFiles(trainingDataFile, saveTrainingFile);
//			Evaluation evalTrain = evaluateDataTrain.calculateEvaluationChunks();
//			evalTrain.printEvaluation();

			System.out.println("\nTest Set: ");
			Data evaluateDataTest = ioHelper.readFiles(testingDataFile, saveTestingFile);
			Evaluation evalTest = evaluateDataTest.calculateEvaluationChunks();
			evalTest.printEvaluation();

			NumberFormat formatter = new DecimalFormat("#0.00");
			System.out.println("\n"
					+ formatter.format(evalTest.getMacroPrecision() * 100)
					+ "\t" + formatter.format(evalTest.getMacroRecall() * 100)
					+ "\t" + formatter.format(evalTest.getMacroFScore() * 100));
			
		} catch (IOException e) {
			// TODO Automatisch generierter Erfassungsblock
			e.printStackTrace();
		}
	}

}
