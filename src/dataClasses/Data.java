package dataClasses;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import classify.Evaluation;


public class Data {
	// List containing all sentences of the data
	private ArrayList<Sentence> data;
	private ArrayList<String> labelList;
	
	
	
	public Data() {
		data = new ArrayList<Sentence>();
	}

	/*
	 * Getter and Setter for data
	 */
	public ArrayList<Sentence> getData() {
		return data;
	}

	public void setData(ArrayList<Sentence> data) {
		this.data = data;
	}
	
	/*
	 * Add a sentence to data
	 */
	public void addSentence(Sentence sentence){
		data.add(sentence);
	}
	
	public int getNumTokens() {
		int numTokens = 0;
		for (Sentence sentence: data) {
			numTokens += sentence.getTokens().size();
		}
		
		return numTokens;
	}
	
	public int getNumSentences() {
		return data.size();
	}
	
	public Sentence getSentence(int sentencePosition) {
		return data.get(sentencePosition);
	}
	
	public void updateToken(int sentencePosition, int tokenPosition, Token updatedToken) {
		Sentence sentence = getSentence(sentencePosition);
		sentence.updateToken(tokenPosition, updatedToken);
		data.remove(sentencePosition);
		data.add(sentencePosition, sentence);
	}
	
	/*
	 * Calculates true positives, false positives and false negatives for all tokens in the data
	 */
	private Map<String, int[]> getTpFpFn() {
		Map<String, int[]> tpFpFn = new HashMap<String, int[]>();
		Map<String, int[]> tpFpFnSentence;
		int[] currentTpFpFn;
		
		// add TP, FP and FN for each of the sentences
		for (Sentence sentence: data) {
			tpFpFnSentence = sentence.getTpFpFn();
			
			// add TP, FP and FN for each of the classes in this sentence
			for (String label: tpFpFnSentence.keySet()) {
				if (tpFpFn.get(label) == null) {
					currentTpFpFn = new int[] {0,0,0};
				} else {
					currentTpFpFn = tpFpFn.get(label);
				}
				
				
				// calculate new TP, FP and FN values
				currentTpFpFn[0] += tpFpFnSentence.get(label)[0];
				currentTpFpFn[1] += tpFpFnSentence.get(label)[1];
				currentTpFpFn[2] += tpFpFnSentence.get(label)[2];
				// update TP, FP and FN of this label
				tpFpFn.put(label, currentTpFpFn);
			}
			
		}
		
		return tpFpFn;
	} 
	
	private Map<String, int[]> getTpFpFnChunks() {
		Map<String, int[]> tpFpFn = new HashMap<String, int[]>();
		Map<String, int[]> tpFpFnSentence;
		int[] currentTpFpFn;
		
		// add TP, FP and FN for each of the sentences
		for (Sentence sentence: data) {
			tpFpFnSentence = sentence.getTpFpFnChunks();
			
			// add TP, FP and FN for each of the classes in this sentence
			for (String label: tpFpFnSentence.keySet()) {
				if (tpFpFn.get(label) == null) {
					currentTpFpFn = new int[] {0,0,0};
				} else {
					currentTpFpFn = tpFpFn.get(label);
				}
				
				
				// calculate new TP, FP and FN values
				currentTpFpFn[0] += tpFpFnSentence.get(label)[0];
				currentTpFpFn[1] += tpFpFnSentence.get(label)[1];
				currentTpFpFn[2] += tpFpFnSentence.get(label)[2];
				// update TP, FP and FN of this label
				tpFpFn.put(label, currentTpFpFn);
			}
			
		}
		return tpFpFn;
	}
	
	
	/*
	 * calculates precision, recall and F-Score for each of the labels,
	 * as well as macro-averaged precision, recall and f-score over all classes
	 * and accuracy (over all classes)
	 */
	public Evaluation calculateEvaluation() {
		Map<String, Double> precisionLabels = new HashMap<String, Double>();
		Map<String, Double> recallLabels = new HashMap<String, Double>();
		Map<String, Double> fScoreLabels = new HashMap<String, Double>();
		
		Map<String, int[]> tpFpFn = this.getTpFpFn();
		
		double precision, recall, fScore;
		int tp, fp, fn;
		int sumTp = 0;
		int sumFp = 0;
		int sumFn = 0;
		
		Evaluation evaluation = new Evaluation();
		
		
		
		// calculate precision, recall and fScore for each of the labels
		for (String label: tpFpFn.keySet()) {
			tp = tpFpFn.get(label)[0];
			fp = tpFpFn.get(label)[1];
			fn = tpFpFn.get(label)[2];

			
			
			sumTp += tp;
			sumFp += fp;
			sumFn += fn;
			// calculate precision
			if (tp != 0 || fp != 0) {
				precision = tp / (double) (tp + fp);		
			} else {
				precision = 0.0;
			}
			
			// calculate recall
			if (tp != 0 || fn != 0) {
				recall = tp / (double) (tp + fn);
			} else {
				recall = 0.0;
			}
			
			// calculate fScore
			if (precision != 0.0 || recall != 0.0) {
				fScore = (2 * precision * recall) / (double) (precision + recall);
			} else {
				fScore = 0.0;
			}
			
			precisionLabels.put(label, precision);
			recallLabels.put(label, recall);
			fScoreLabels.put(label, fScore);
		}
		evaluation.setPrecisionForLabels(precisionLabels);
		evaluation.setRecallForLabels(recallLabels);
		evaluation.setfScoreForLabels(fScoreLabels);
		
		double sumPrecision = 0;
		double sumRecall = 0;
		double sumFScore = 0;
		// calculate macro-averaged precision, recall and fScore
		for (String label: tpFpFn.keySet()) {
			sumPrecision += precisionLabels.get(label);
			sumRecall += recallLabels.get(label);
			sumFScore += fScoreLabels.get(label);
		}
		double macroPrecision = sumPrecision / precisionLabels.size();
		double macroRecall = sumRecall / recallLabels.size();
		double macroFScore = sumFScore / fScoreLabels.size();
		
		evaluation.setMacroPrecision(macroPrecision);
		evaluation.setMacroRecall(macroRecall);
		evaluation.setMacroFScore(macroFScore);
		
		// calculate accuracy
		double accuracy = sumTp / (double) this.getNumTokens();
		evaluation.setAccuracy(accuracy);
		
		return evaluation;
	}
	
	
	public Evaluation calculateEvaluationChunks() {
		Map<String, Double> precisionLabels = new HashMap<String, Double>();
		Map<String, Double> recallLabels = new HashMap<String, Double>();
		Map<String, Double> fScoreLabels = new HashMap<String, Double>();
		
		Map<String, int[]> tpFpFn = this.getTpFpFnChunks();
		
		double precision, recall, fScore;
		int tp, fp, fn;
		int sumTp = 0;
		int sumFp = 0;
		int sumFn = 0;
		
		Evaluation evaluation = new Evaluation();
		
		// calculate precision, recall and fScore for each of the types
		for (String label: tpFpFn.keySet()) {
			tp = tpFpFn.get(label)[0];
			fp = tpFpFn.get(label)[1];
			fn = tpFpFn.get(label)[2];

			sumTp += tp;
			sumFp += fp;
			sumFn += fn;
			// calculate precision
			if (tp != 0 || fp != 0) {
				precision = tp / (double) (tp + fp);		
			} else {
				precision = 0.0;
			}
			
			// calculate recall
			if (tp != 0 || fn != 0) {
				recall = tp / (double) (tp + fn);
			} else {
				recall = 0.0;
			}
			
			// calculate fScore
			if (precision != 0.0 || recall != 0.0) {
				fScore = (2 * precision * recall) / (double) (precision + recall);
			} else {
				fScore = 0.0;
			}
			
			precisionLabels.put(label, precision);
			recallLabels.put(label, recall);
			fScoreLabels.put(label, fScore);
		}
		evaluation.setPrecisionForLabels(precisionLabels);
		evaluation.setRecallForLabels(recallLabels);
		evaluation.setfScoreForLabels(fScoreLabels);
		
		double sumPrecision = 0;
		double sumRecall = 0;
		double sumFScore = 0;
		// calculate macro-averaged precision, recall and fScore
		for (String label: tpFpFn.keySet()) {
			sumPrecision += precisionLabels.get(label);
			sumRecall += recallLabels.get(label);
			sumFScore += fScoreLabels.get(label);
		}
		double macroPrecision = sumPrecision / precisionLabels.size();
		double macroRecall = sumRecall / recallLabels.size();
		double macroFScore = sumFScore / fScoreLabels.size();
		
		evaluation.setMacroPrecision(macroPrecision);
		evaluation.setMacroRecall(macroRecall);
		evaluation.setMacroFScore(macroFScore);
		
		// horrible runtime
		// calculate accuracy
//		double accuracy = calculateEvaluation().getAccuracy();
//		evaluation.setAccuracy(accuracy);
		
		return evaluation;
	}
	
	
	
	public ArrayList<String> getLabels() {
		labelList = new ArrayList<String>();
		
		for (Sentence sentence: data) {
			
		}
		
		return this.labelList;
	}
	
}
