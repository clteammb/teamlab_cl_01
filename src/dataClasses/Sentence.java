package dataClasses;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class Sentence {
	// List containing all tokens of the sentence
	private ArrayList<Token> tokens;

	public Sentence() {
		tokens = new ArrayList<Token>();
	}

	/*
	 * Getter and Setter for the list of tokens of this sentence
	 */
	public ArrayList<Token> getTokens() {
		return tokens;
	}

	public void setTokens(ArrayList<Token> tokens) {
		this.tokens = tokens;
	}
	
	/*
	 * Add a token to this sentence
	 */
	public void addToken(Token token) {
		tokens.add(token);
	}
	
	/*
	 * returns the token at the given position of the sentence 
	 * (starting with 0 for the first token)
	 */
	public Token getToken(int tokenPosition) {
		return tokens.get(tokenPosition);
	}
	
	/*
	 * returns the number of tokens in the sentence
	 */
	public int getNumTokens() {
		return tokens.size();
	}
	
	void updateToken(int tokenPosition, Token updatedToken) {
		tokens.remove(tokenPosition);
		tokens.add(tokenPosition, updatedToken);
	}

	/*
	 * calculates the true positives, false positives and false negatives
	 * for each label in the sentence and returns a HashMap (Label: TP/FP/FN)
	 */
	public Map<String, int[]> getTpFpFn() {
		Map<String, int[]> tpFpFn = new HashMap<String, int[]>();
		int[] tpFpFnLabel;
		
		// Add TP, FP and FN for each token in the sentence
		for (Token token: tokens) {
			// True positives
			if (token.getGoldTag().equals(token.getPredTag())) {
				if (tpFpFn.get(token.getGoldTag()) == null) {
					tpFpFnLabel = new int[] {0,0,0};
				} else {
					tpFpFnLabel = tpFpFn.get(token.getGoldTag());
				}
				// update TP count
				tpFpFnLabel[0] += 1;
				tpFpFn.put(token.getGoldTag(), tpFpFnLabel);
			} else {
				// False positives
				if (tpFpFn.get(token.getPredTag()) == null) {
					tpFpFnLabel = new int[] {0,0,0};
					tpFpFn.put(token.getPredTag(), tpFpFnLabel);
				} else {
					tpFpFnLabel = tpFpFn.get(token.getPredTag());
				}
				// update FP count
				tpFpFnLabel[1] += 1;
				tpFpFn.put(token.getPredTag(), tpFpFnLabel);
				// False negatives
				if (tpFpFn.get(token.getGoldTag()) == null) {
					tpFpFnLabel = new int[] {0,0,0};
				} else {
					tpFpFnLabel = tpFpFn.get(token.getGoldTag());
				}
				// update FN count
				tpFpFnLabel[2] += 1;
				tpFpFn.put(token.getGoldTag(), tpFpFnLabel);
			}
		}
		
		return tpFpFn;
	}
	
	public Map<String, int[]> getTpFpFnChunks() {
		Map<String, int[]> tpFpFn = new HashMap<String, int[]>();
		int[] tpFpFnType;
		
		// Find chunks in gold standard
		Map<String, ArrayList<String>> goldChunks = createGoldChunks();
		Map<String, ArrayList<String>> predChunks = createPredictedChunks();
		
		ArrayList<String> gold;
		ArrayList<String> pred;
		
		// Compute true positives, false positives and false negatives;
		for (String type: goldChunks.keySet()) {
			gold = goldChunks.get(type);
			pred = predChunks.get(type);
			
			if (gold == null) {
				gold = new ArrayList<String>();
			}
			
			if (pred == null) {
				pred = new ArrayList<String>();
			}
			
			
			tpFpFnType = new int[3];
			
 			for (String chunk: gold) {
 				// true positives - in both sets
 				if (pred.contains(chunk)) {
 					tpFpFnType[0] += 1;
 				// false negatives - only in gold set
 				} else {
 					tpFpFnType[2] += 1;
 				}
 			}
 			
 			for (String chunk: pred) {
 				// false positives - only in predicted set
 				if (!gold.contains(chunk)) {
 					tpFpFnType[1] += 1;
 				}
 			}
			
			tpFpFn.put(type, tpFpFnType);
		}
		
		return tpFpFn;
	}
	
	
	private Map<String, ArrayList<String>> createGoldChunks() {
		String sequence = null;
		String currentChunk = null;
		ArrayList<String> chunkList;
		Map<String, ArrayList<String>> goldChunks = new HashMap<String, ArrayList<String>>(); 
		String goldTag;
		
		// Find chunks in gold standard
		for (Token token: this.getTokens()) {
			goldTag = token.getGoldTag();

			// B-Tag
			if (goldTag.startsWith("B")) {
				// another sequence before needs to be added as chunk
				if (sequence != null) {
					if (!goldChunks.containsKey(sequence)) {
						goldChunks.put(sequence, new ArrayList<String>());
					}
					chunkList = goldChunks.get(sequence);
					chunkList.add(currentChunk);
					goldChunks.put(sequence, chunkList);
				}
				// save type of chunk
				sequence = goldTag.split("-")[1];
				// start new chunk form
				currentChunk = token.getWordform();

				// I-Tag
			} else if (goldTag.startsWith("I")) {
				// previous chunk is continued
				if (goldTag.split("-")[1].equals(sequence)) {
					currentChunk += " " + token.getWordform();
					// different chunk starts
				} else {
					// another sequence before needs to be added as chunk
					if (sequence != null) {
						if (!goldChunks.containsKey(sequence)) {
							goldChunks.put(sequence, new ArrayList<String>());
						}
						chunkList = goldChunks.get(sequence);
						chunkList.add(currentChunk);
						goldChunks.put(sequence, chunkList);
					}
					// save type of chunk
					sequence = goldTag.split("-")[1];
					// start new chunk form
					currentChunk = token.getWordform();

				}

				// O-Tag
			} else {
				// previous chunk needs to be added to the chunk list
				if (sequence != null) {
					if (!goldChunks.containsKey(sequence)) {
						goldChunks.put(sequence, new ArrayList<String>());
					}
					chunkList = goldChunks.get(sequence);
					chunkList.add(currentChunk);
					goldChunks.put(sequence, chunkList);
				}
				sequence = null;
			}

		}
		return goldChunks;
	}
	
	private Map<String, ArrayList<String>> createPredictedChunks() {
		String sequence = null;
		String currentChunk = null;
		ArrayList<String> chunkList;
		Map<String, ArrayList<String>> predictedChunks = new HashMap<String, ArrayList<String>>(); 
		String predTag;
		
		// Find chunks in prediction
		for (Token token: this.getTokens()) {
			predTag = token.getPredTag();

			// B-Tag
			if (predTag.startsWith("B")) {
				// another sequence before needs to be added as chunk
				if (sequence != null) {
					if (!predictedChunks.containsKey(sequence)) {
						predictedChunks.put(sequence, new ArrayList<String>());
					}
					chunkList = predictedChunks.get(sequence);
					chunkList.add(currentChunk);
					predictedChunks.put(sequence, chunkList);
				}
				// save type of chunk
				sequence = predTag.split("-")[1];
				// start new chunk form
				currentChunk = token.getWordform();

				// I-Tag
			} else if (predTag.startsWith("I")) {
				// previous chunk is continued
				if (predTag.split("-")[1].equals(sequence)) {
					currentChunk += " " + token.getWordform();
					// different chunk starts
				} else {
					// another sequence before needs to be added as chunk
					if (sequence != null) {
						if (!predictedChunks.containsKey(sequence)) {
							predictedChunks.put(sequence, new ArrayList<String>());
						}
						chunkList = predictedChunks.get(sequence);
						chunkList.add(currentChunk);
						predictedChunks.put(sequence, chunkList);
					}
					// save type of chunk
					sequence = predTag.split("-")[1];
					// start new chunk form
					currentChunk = token.getWordform();

				}

				// O-Tag
			} else {
				// previous chunk needs to be added to the chunk list
				if (sequence != null) {
					if (!predictedChunks.containsKey(sequence)) {
						predictedChunks.put(sequence, new ArrayList<String>());
					}
					chunkList = predictedChunks.get(sequence);
					chunkList.add(currentChunk);
					predictedChunks.put(sequence, chunkList);
				}
				sequence = null;
			}

		}
		return predictedChunks;
	}
	
}
