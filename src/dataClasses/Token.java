package dataClasses;
import java.io.Serializable;
import java.util.ArrayList;


public class Token implements Serializable {
	
	private String wordform; // word form of the token
	private String goldTag; // gold (correct) tag of the token
	private String predTag; // predicted tag of the token
	
	private ArrayList<Integer> featureVector;
	
	/*
	 * Getters and Setters
	 */
	public String getWordform() {
		return wordform;
	}
	public void setWordform(String wordform) {
		this.wordform = wordform;
	}
	public String getGoldTag() {
		return goldTag;
	}
	public void setGoldTag(String goldTag) {
		this.goldTag = goldTag;
	}
	public String getPredTag() {
		return predTag;
	}
	public void setPredTag(String predTag) {
		this.predTag = predTag;
	}
	public ArrayList<Integer> getFeatureVector() {
		return featureVector;
	}
	public void setFeatureVector(ArrayList<Integer> featureVector) {
		this.featureVector = featureVector;
	}
	
	

}
