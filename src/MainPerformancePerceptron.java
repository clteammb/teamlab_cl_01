import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import classify.Evaluation;
import classify.MultilabelPerceptron;
import dataClasses.Data;
import helperClasses.IOHelper;


public class MainPerformancePerceptron {
	
	public static void main(String[] args) {
		
		boolean posTagging = true;
		boolean geneTagging = true;
	
		if(posTagging) {
		// Pos Tagging
		String trainingDataFile = "/home/users0/klingern/teaching/team-lab-ss2015/data/pos/train.col";
		String testingDataFile = "/home/users0/klingern/teaching/team-lab-ss2015/data/pos/test.col";

		String folder = "/home/users0/rothmube/teamlab/models/";
		String saveTrainingFile = "/home/users0/rothmube/teamlab/data/resultsPos_train.col"; 
		String saveTestingFile = "/home/users0/rothmube/teamlab/data/resultsPos_development.col";

		int posIterationsCount = 10;


		try {
			IOHelper ioHelper = new IOHelper();
			PrintStream printStream = new PrintStream(new File("/home/users0/rothmube/git/teamlab_cl_01/experiments/performance/perceptronPerformancePOS.txt"));


			Data trainingData = ioHelper.readFiles(trainingDataFile, trainingDataFile);

			MultilabelPerceptron mp = new MultilabelPerceptron(true);
			mp.trainPerceptron(trainingData, posIterationsCount);

			mp.saveModel( folder  + "normal_features-pos.txt", folder + "normal_perceptronModel-pos.txt");

			//trainingData = ioHelper.readTokenFile(trainingDataFile);
			Data testingData = ioHelper.readTokenFile(testingDataFile);
			//trainingData = mp.classifyData(trainingData);
			testingData = mp.classifyData(testingData);

			//ioHelper.saveData(saveTrainingFile, trainingData, false);
			ioHelper.saveData(saveTestingFile, testingData, false);


			System.out.println("\nTest Set: ");
			Data evaluateDataTest = ioHelper.readFiles(testingDataFile, saveTestingFile);
			Evaluation evalTest = evaluateDataTest.calculateEvaluation();
			evalTest.printEvaluation();

			NumberFormat formatter = new DecimalFormat("#0.00");
			printStream.println(formatter.format(evalTest.getMacroPrecision() * 100)
					+ "\t" + formatter.format(evalTest.getMacroRecall() * 100)
					+ "\t" + formatter.format(evalTest.getMacroFScore() * 100)
					+ "\t" + formatter.format(evalTest.getAccuracy() * 100)
					+ "\n");



			for(int i=1; i<=10; i++ ) {
				trainingData = ioHelper.readFiles(trainingDataFile, trainingDataFile);

				mp = new MultilabelPerceptron(true);
				mp.trainRandomizedPerceptron(trainingData, posIterationsCount);

				mp.saveModel( folder + i + "features-pos.txt", folder+i+ "perceptronModel-pos.txt");

				//trainingData = ioHelper.readTokenFile(trainingDataFile);
				testingData = ioHelper.readTokenFile(testingDataFile);
				//trainingData = mp.classifyData(trainingData);
				testingData = mp.classifyData(testingData);

				//ioHelper.saveData(saveTrainingFile, trainingData, false);
				ioHelper.saveData(saveTestingFile, testingData, false);


				System.out.println("\nTest Set: ");
				evaluateDataTest = ioHelper.readFiles(testingDataFile, saveTestingFile);
				evalTest = evaluateDataTest.calculateEvaluation();
				evalTest.printEvaluation();

				printStream.println(formatter.format(evalTest.getMacroPrecision() * 100)
						+ "\t" + formatter.format(evalTest.getMacroRecall() * 100)
						+ "\t" + formatter.format(evalTest.getMacroFScore() * 100)
						+ "\t" + formatter.format(evalTest.getAccuracy() * 100));


			}

		} catch (IOException e) {
			// TODO Automatisch generierter Erfassungsblock
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Automatisch generierter Erfassungsblock
			e.printStackTrace();
		}
		}
		
		if (geneTagging){
		
		// Genes Tagging
		String trainingDataFile = "/home/users0/rothmube/teamlab/data/GeneExtraction/80_train.iob";
		String testingDataFile = "/home/users0/rothmube/teamlab/data/GeneExtraction/test.iob";
		
		String saveTrainingFile = "/home/users0/rothmube/teamlab/data/resultsGene_train.iob"; 
		String saveTestingFile = "/home/users0/rothmube/teamlab/data/resultsGene_development.iob";
		
		
//		trainingDataFile = "/home/users0/klingern/teaching/team-lab-ss2015/data/pos/train.col";
//		testingDataFile = "/home/users0/klingern/teaching/team-lab-ss2015/data/pos/test.col";
//
		String folder = "/home/users0/rothmube/teamlab/models/";
//		saveTrainingFile = "/home/users0/rothmube/teamlab/data/resultsPos_train.col"; 
//		saveTestingFile = "/home/users0/rothmube/teamlab/data/resultsPos_development.col";

		int genesIterationsCount = 10;


		try {
			IOHelper ioHelper = new IOHelper();
			PrintStream printStream = new PrintStream(new File("/home/users0/rothmube/git/teamlab_cl_01/experiments/performance/perceptronPerformanceGenes.txt"));


			Data trainingData = ioHelper.readFiles(trainingDataFile, trainingDataFile);

			MultilabelPerceptron mp = new MultilabelPerceptron(false);
			mp.trainPerceptron(trainingData, genesIterationsCount);

			mp.saveModel( folder  + "normal_features-genes.txt", folder + "normal_perceptronModel-genes.txt");

			//trainingData = ioHelper.readTokenFile(trainingDataFile);
			Data testingData = ioHelper.readTokenFile(testingDataFile);
			//trainingData = mp.classifyData(trainingData);
			testingData = mp.classifyData(testingData);

			//ioHelper.saveData(saveTrainingFile, trainingData, false);
			ioHelper.saveData(saveTestingFile, testingData, false);


			System.out.println("\nTest Set: ");
			Data evaluateDataTest = ioHelper.readFiles(testingDataFile, saveTestingFile);
			Evaluation evalTest = evaluateDataTest.calculateEvaluation();
			evalTest.printEvaluation();

			NumberFormat formatter = new DecimalFormat("#0.00");
			printStream.println(formatter.format(evalTest.getMacroPrecision() * 100)
					+ "\t" + formatter.format(evalTest.getMacroRecall() * 100)
					+ "\t" + formatter.format(evalTest.getMacroFScore() * 100)
					+ "\t" + formatter.format(evalTest.getAccuracy() * 100)
					+ "\n");



			for(int i=1; i<=10; i++ ) {
				trainingData = ioHelper.readFiles(trainingDataFile, trainingDataFile);

				mp = new MultilabelPerceptron(false);
				mp.trainRandomizedPerceptron(trainingData, genesIterationsCount);

				mp.saveModel( folder + i + "features-genes.txt", folder+i+ "perceptronModel-genes.txt");

				//trainingData = ioHelper.readTokenFile(trainingDataFile);
				testingData = ioHelper.readTokenFile(testingDataFile);
				//trainingData = mp.classifyData(trainingData);
				testingData = mp.classifyData(testingData);

				//ioHelper.saveData(saveTrainingFile, trainingData, false);
				ioHelper.saveData(saveTestingFile, testingData, false);


				System.out.println("\nTest Set: ");
				evaluateDataTest = ioHelper.readFiles(testingDataFile, saveTestingFile);
				evalTest = evaluateDataTest.calculateEvaluation();
				evalTest.printEvaluation();

				printStream.println(formatter.format(evalTest.getMacroPrecision() * 100)
						+ "\t" + formatter.format(evalTest.getMacroRecall() * 100)
						+ "\t" + formatter.format(evalTest.getMacroFScore() * 100)
						+ "\t" + formatter.format(evalTest.getAccuracy() * 100));


			}

		} catch (IOException e) {
			// TODO Automatisch generierter Erfassungsblock
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Automatisch generierter Erfassungsblock
			e.printStackTrace();
		}
		}

	}

}
