import helperClasses.IOHelper;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import classify.Evaluation;
import classify.MultilabelPerceptron;
import dataClasses.Data;


public class MainPOSTagging {

	public static void main(String[] args) {
		// TODO Automatisch generierter Methodenstub

		String trainingDataFile = "/home/users0/klingern/teaching/team-lab-ss2015/data/pos/train.col";
		String testingDataFile = "/home/users0/klingern/teaching/team-lab-ss2015/data/pos/dev-predicted.col";
		
		String saveTrainingFile = "/home/users0/rothmube/teamlab/data/resultsPos_train.col"; 
		String saveTestingFile = "/home/users0/rothmube/teamlab/data/resultsPos_development.col";
		
		IOHelper ioHelper = new IOHelper();
		try {
			Data trainingData = ioHelper.readFiles(trainingDataFile, trainingDataFile);
							
			MultilabelPerceptron mp = new MultilabelPerceptron(true);
			mp.trainPerceptron(trainingData, 10);
			
			mp.saveModel("/home/users0/rothmube/teamlab/models/features-pos.txt", "/home/users0/rothmube/teamlab/models/perceptronModel-pos.txt");
			
			//trainingData = ioHelper.readTokenFile(trainingDataFile);
			Data testingData = ioHelper.readTokenFile(testingDataFile);
			trainingData = mp.classifyData(trainingData);
			testingData = mp.classifyData(testingData);

			//ioHelper.saveData(saveTrainingFile, trainingData, false);
			ioHelper.saveData(saveTestingFile, testingData, false);
			
		} catch (IOException e) {
			// TODO Automatisch generierter Erfassungsblock
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Automatisch generierter Erfassungsblock
			e.printStackTrace();
		}
		
		Data evaluateDataTrain;
		try {
//			System.out.println("Training Set: ");
//			evaluateDataTrain = ioHelper.readFiles(trainingDataFile, saveTrainingFile);
//			Evaluation evalTrain = evaluateDataTrain.calculateEvaluation();
//			evalTrain.printEvaluation();

			System.out.println("\nTest Set: ");
			Data evaluateDataTest = ioHelper.readFiles(testingDataFile, saveTestingFile);
			Evaluation evalTest = evaluateDataTest.calculateEvaluation();
			evalTest.printEvaluation();

			NumberFormat formatter = new DecimalFormat("#0.00");
			System.out.println("\n"
					+ formatter.format(evalTest.getMacroPrecision() * 100)
					+ "\t" + formatter.format(evalTest.getMacroRecall() * 100)
					+ "\t" + formatter.format(evalTest.getMacroFScore() * 100)
					+ "\t" + formatter.format(evalTest.getAccuracy() * 100));
			
		} catch (IOException e) {
			// TODO Automatisch generierter Erfassungsblock
			e.printStackTrace();
		}
	}

}
